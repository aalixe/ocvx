# OCVX

Third year project at Epita

# Look at the project

There is some display problems with notebooks when looking directly from the web or from gitlab, 
even some parts of the notebooks are missing when looking like this. Please refer to the following parts.
We strongly recommand using jupyter notebooks

### With jupyter notebook

1. Clone the repository
2. Install the requirements (requirements.txt) with 'pip install -r requirements.txt'
3. Open the notebooks present in the 'src' directory with a jupyter notebook

### With Google Collab

1. Don't forget to upload the 'img' folder, otherwise you won't be able to see any images.