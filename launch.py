#! /usr/bin/python3
import os
import sys
from scipy.optimize import newton
from tests import newton as nt
from tests import test_parsor as tp


path = "tests/*"


def run_C1(ctest):
	try:
		for gamma in range(10, 100, 10):
			ctest.gamma = gamma
			res = nt.Newton(ctest, 0.5, 1e-6, 1000)
			print("- with gamma = " + str(gamma) + ": " + str(res))
			try:
				print("From scipy: " + str(newton(ctest.value,x0=0.5,tol=1e-6,maxiter=1000)))
			except RuntimeError as e:
				print("From scipy: " + str(e))
		return 0
	except Exception as e:
		print("Unexpected error:", sys.exc_info()[0])
		print(e)
		return 1

def run_C2(ctest):
#	try:
	for gamma in range(10, 20, 50):
		ctest.gamma = gamma
		for alpha in range(10, 20, 50):
			ctest.alpha = alpha
			res = nt.Newton(ctest, 0.5, 1e-6, 1000)
			print("- with gamma, alpha = " + str(gamma) + "," + str(alpha) +" : " + str(res))
			try:
				print("From scipy: " + str(newton(ctest.value,x0=0.5,tol=1e-6,maxiter=1000)))
			except RuntimeError as e:
				print("From scipy: " + str(e))
	return 0
	#!except Exception as e:
	#!	print("Unexpected error:", sys.exc_info()[0])
	#!	print(e)
	#!	return 1


"""
Run Test

1. Build the test
2. Check wether it is a 2 or 1 coef eq
3. run the test on newton
"""
def run_test(test):
	print("+-- test: " + os.path.basename(os.path.normpath(test)))
	print("|")
	ctest = tp.buildTest(test)
	print(ctest)
	res = None
	print("Running Newton method:")
	if ctest.nb_var == 2:
		res = run_C2(ctest)
	else:
		res = run_C1(ctest)
	print(res)


"""
Launch Test

Read all the tests in a directory and launch them
"""
def launch_test(list_dir):
	for dir in list_dir:
		print("+---- Directory: " + os.path.basename(os.path.normpath(dir)))
		print("|")
		tests = tp.readDir(dir)
		for test in tests:
			run_test(test)
		print("|")
		print("+----\n")


"""
Main

1. Read directories in Path 
2. Call the Test routine 
"""
def main():
	directories = tp.listDir(path)
	launch_test(directories)	
	return 0

main()