from scipy.misc import derivative
from numpy.linalg import inv,norm
import numpy as np
from scipy.optimize import newton

def newton_method(f, a=0.5, nb_iter=1000):
    for i in range(nb_iter):
        a = a - (misc.derivative(f, a) / f(a))
    return a

def Newton(function,x0,eps= 1e-6,maxiter=1000,viz = True):
    f = function.value
    x = np.array(x0,dtype='float64').reshape(1,function.nb_params)
    Gr = function.grad(x).reshape(function.nb_params,1)

    #print("Gr is :", Gr)
    #print("Hess is :",invhess(function,x))
    hess = function.hess(x).reshape(function.nb_params,function.nb_params)
    #print("hess sh:",inv(hess))
    deltaX = -inv(hess) @ Gr
    it = 0
    iters_dir,iters = deltaX,x
    norme = norm(Gr.T)/2
    while norme > eps and it < maxiter:
        Gr = function.grad(x).reshape(function.nb_params,1)
        hess = function.hess(x).reshape(function.nb_params,function.nb_params)
        deltaX = -inv(hess) @ Gr
        x += deltaX.T
        iters_dir = np.vstack([iters_dir, deltaX])
        iters = np.vstack([iters, x])
        norme = norm(Gr.T)/2
        it += 1
    if viz:
        vis = visualisation()
        print("iterdir shape:",iters_dir.shape)
        #vis.plotDescent(function,iters,iters_dir
        vis(function,iters,iters_dir.reshape(-1,2))
    #print(norme,Gr)
    return x,it


#mothode de newton avec contraint
from scipy.linalg import null_space
from numpy.linalg import pinv
import numpy as np
#les contraints doivent etre représenté sous la forme [a,c,X0] si pour tout x dans E : ax = c et X0 et une solution parrticulier  
class Newton_with_constraint():
    def __init__(self, test_function):
        self.test_function = test_function
    def GetParametriqueRepesentation(self):
        constraints = np.array(self.test_function.constraint)
        A = np.zeros((constraints.shape[0],self.test_function.nb_params))
        b = np.zeros((constraints.shape[0],1))
        for i in range(constraints.shape[0]):
            A[i,:] = constraints[i,0].reshape(1,-1)
            b[i,:] = constraints[i,1].reshape(1,-1)
        F = null_space(A)
        x0 = pinv(A) @ b
        transformation = lambda z: x0 + F@z
        return transformation
    def Solve(self,):
        transformation = GetParametriqueRepesentation()
        test_function.value = test_function.value(transformation)
        x,it = Newton(test_function,x0,eps= 1e-6,maxiter=1000)
        return transformation(x),it