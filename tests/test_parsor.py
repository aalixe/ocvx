import os
import glob

class Test_func():

	def __init__(self, name, value, dim, nb_params, convex=None, grad=None, hess=None, HCN=None, constraints=[], nb_var=1):
		#Test name
		self.name = name
		#How many coef are used in the function
		self.nb_var = int(nb_var)
		#The function translation from string
		self.str_value = value
		if self.nb_var == 1:
			self.eval_gamma = (lambda gamma, x: eval(value))
			self.value = (lambda x : self.eval_gamma(self.gamma, x))
		else:
			self.eval_gamma = (lambda alpha, gamma, x : eval(value))
			self.value = (lambda x : self.eval_gamma(self.alpha, self.gamma, x))
		#Number of dimension
		self.dim = int(dim)
		#Number of parameters
		self.nb_params = int(nb_params)
		#Is the function convex
		self.convex = bool(convex)
		#Gradient
		self.grad = grad
		#Hessienne
		self.hess = hess
		self.HCN = HCN
		#Coef value
		self.gamma = 0
		self.alpha = 0
		#Constraints
		self.str_constraints = constraints
		self.constraints = [lambda gamma, x, f=f : eval(f) for f in constraints]


	def __str__(self):
		return "Test_func \'" + self.str_value + "\' at " + hex(id(self)) + " containing:\n" + str(self.__dict__)

def listDir(path):
	return [f for f in glob.glob(path) if os.path.isdir(f) and "__pycache__" not in f]

def readDir(dir):
	return [f for f in glob.glob(dir + "/*")]

def readTest(path):
	with open(path, 'r') as f:
		return {elem.split("@")[0]:elem.split("@")[1] for elem in f.read().split('\n')}

def dicToTest(test):
	try:
		test['constraints'] = test['constraints'].split(';')
	except Exception :
		print("No constraints")
	return Test_func(**test)

def buildTest(path):
	return dicToTest(readTest(path))

#This is a test
"""
test = dicToTest(readTest("multi_dim/multi.txt"))
print(test.constraints)
for t in test.constraints:
	print(t(8, 5))
"""























